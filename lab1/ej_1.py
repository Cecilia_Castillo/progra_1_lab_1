#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Función para sacar el promedio

try:
    numb1 = float(input("Ingrese un numero "))
    numb2 = float(input("Ingrese un segundo numero "))
    numb3 = float(input("Ingrese un tercer numero "))

    lista = [numb1, numb2, numb3]

    def promedio(lista):
        prom = sum(lista) / 3
        print("Su promedio es", prom)
    promedio(lista)
    
except ValueError:
    print("El valor no es válido")

except TypeError:
    print("El tipo de dato ingresado no es válido")

# Función para el cuadrado de una lista

try:
    numb1 = float(input("Ingrese un numero "))
    numb2 = float(input("Ingrese un segundo numero "))
    numb3 = float(input("Ingrese un tercer numero "))


    lista = [numb1, numb2, numb3]

    def cuadrado(lista):
        for i in lista:
            print ("El cuadrado es", i ** 2)

    cuadrado(lista)

except ValueError:
    print("El valor no es válido")

except TypeError:
    print("El tipo de dato ingresado no es válido")

# Función para encontrar la palabra más larga

a = str(input("Ingrese una palabra "))
b = str(input("Ingrese otra palabra "))
c = str(input("Ingrese otra palabra más "))

lista_pal = [a, b, c]

def ordenar(lista_pal):
    lista_pal.sort()
    print ("La palabra más larga es", lista_pal[2])

ordenar(lista_pal)
